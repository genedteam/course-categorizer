package com.gened;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NlpDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NlpDemoApplication.class, args);
	}
}
