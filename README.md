# Course Categorizer #

The Course Categorizer runs Apache OpenNLP's Document Categorizer to place GenEd courses into 10 categories. The app utilizes Springboot as a means to provide a JSON interchange with other services. 

Version 0.01

This repo contains the serialized model needed to categorize courses. For non-binary GenEd data sources please see (https://bitbucket.org/genedteam/gened-course-data)

### How do I get set up? ###

* Intall Java
* Clone the repo
* Install Maven dependencies 
* Run the app

MIT License